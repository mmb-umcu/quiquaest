##   QuiQuaEst: Quick quality estimation for WGS

QuiQuaEst is a Quick Quality Estimation tool for Illumina sequencing
samples for Whole genome sequencing (e.g. for the bactofidia pipeline).

It assumes paired end sequencing data, calculates the file size of all forward and reverse reads together,
and calculates a rough maximum coverage estimate for a theoretical 1 Mbp genome.

It also gives an error message if the compressed input file is corrupt

Download the script with

  git clone https://gitlab.com/mmb-umcu/quiquaest

Change into the directory

  cd quiquaest
  
Generate a list of your sequencing files to check with

  ls  data*/*gz > list-all.txt
  cut -f 1,2 -d _  list-all.txt | sort | sort -u > list_all_uniq.txt

Run QuiQuaEst with

  ./QuiQuaEst.sh  list_all_uniq.txt 

Its output is a table which indicates:

  - The size of all files belonging to sample X
  - Total number of bases larger than Q20
  - Theoretical maximum coverage for 1 Mbp genome for sample X

The output is written to a table (quiquaest.tsv).
