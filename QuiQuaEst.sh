#!/bin/bash
#- QC for file integrity
#- QC for file size
#- QC for quality

# takes a list of input files

##1. Checks
##Check for command line arguments

if [ $# -eq 0 -o "$1" == "-h" -o "$1" == "--help" ]; then
    echo "
###########################################################################
############   QuiQuaEst: Quick quality estimation for WGS ################
##                                                                       ##
##                                                                       ##
## A list of the path of WGS samples containing paired end,              ##
## compressed sequencing files (fastq.gz)                                ## 
## must be present in the same folder from where the script is called.   ##
##                                                                       ##
##                                                                       ##
## Example:                                                              ##
##                                                                       ##
## ./QuiQuaEst.sh  list_all_uniq.txt                                     ##                                               
##                                                                       ##
##  The list can be produced with:                                       ##
##  ls  data*/*gz > list-all.txt                                         ##
##  cut -f 1,2 -d "_"  list-all.txt | sort | sort -u > list_all_uniq.txt ##
##                                                                       ##
##  Version Nov2019                                                       ##
###########################################################################"
    exit
fi

## Check for list

if [ "$1" == """ALL""" ];then
   echo "All found lists will be processed"
   files=(./list*)
else
   echo else
   files=( "$@" )
fi


for file in "${files[@]}"
do
  if [ -e "$file" ]
   then   # Check whether file exists.
     echo 'Found files for ' "$file" 
   else
     echo 'List as "$file" is missing in this folder.
  Please execute this script from the location of the list with paths to the sequencing files.
 Exiting.' 2>&1
 exit 1
   fi
done

out=$(pwd)/quiquaest_out.tsv 
rm -f ${out}
touch ${out}

##Check is seqtk is available
if command -v seqtk > /dev/null
 then
    echo 'seqtk found'
 else 
  echo 'seqtk missing. Please install seqtk'
  exit 1
fi


for i in  "${files[@]}"
do
echo "processing" "$i"
echo 
echo
echo "The output is a table " ${out} " which indicates:"
echo 
echo "  - The size of all files belonging to each"
echo "  - Total number of bases larger than Q20"
echo "  - Theoretical maximum coverage for a 1 Mbp genome for each"

echo 

echo -e "Sample"' \t '"Size"' \t '">Q20"' \t '"Cov/Mbp" 2>&1 | tee -a "${out}"
while read  -u 10 LINE
    do
#Filename
      echo -n -e "$LINE" ' \t ' 2>&1 | tee -a "$out"
#total size of runs belonging to sample
      echo -n -e $(zcat ${LINE}* | wc -c) ' \t ' 2>&1 | tee -a "${out}"
  new=new.fastq
  rm new.fastq
  touch new.fastq
  for i in ${LINE}*
    do
    zcat "$i" >> ${new}
    done

  ### find all  bases > Phred score 20
  total=$(seqtk fqchk ${new} | head -n 3 | tail -n 1 | cut -f 2,2)

  #### Trim leading whitespaces ###
  total="${total##*( )}"
 
  ### trim trailing whitespaces  ##
  total="${total%%*( )}"
#Total number of bases > Q20
  echo -n -e ${total} ' \t ' 2>&1 | tee -a "${out}"
  ### convert to int
  total=$((total | bc -l))

  ####calculate coverage for 1Mbp genome
  onemio=1000000
  mbp=$(($total / $onemio | bc -l))
#Theoretical maximum coverage for 1 Mbp genome for sample
  echo -e $mbp ' \t ' 2>&1 | tee -a "${out}"

  done 10< ${i}
done


echo "Recommended is a theoretical maximum coverage of at least 20 (for the expected genome size) to ensure successful execution in the bactofidia assembly pipeline"

